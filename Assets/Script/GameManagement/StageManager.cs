using UnityEngine;
using UnityEngine.SceneManagement;

namespace Script.GameManagement
{
    public class StageManager : MonoBehaviour
    {
        
        [SerializeField] private Transform playerTran;
        [SerializeField] public float dieTimeCount;
        [SerializeField] private GameObject pauseMenu;

        void Start()
        {
            Debug.Assert(playerTran == null,"playerTran = null");
            Debug.Assert(pauseMenu == null,"pauseMenu game object = null");

        }
        private void Update()
        {
            PlayerRebornCheck();
        }

        void PlayerRebornCheck()
        {
            if (playerTran.position.y < -20)
            {
                SoundManager.Instance.PlayerFall();
                Debug.Log("you die, it's " + dieTimeCount);
                playerTran.position = Vector3.zero;

                dieTimeCount += 1;
            }
        }
        
        public void LoadMainMenu()
        {
            SoundManager.Instance.PlayButton();
            SceneManager.LoadScene(0);
        }
        
        public void OpenPauseMenu()
        {
            Time.timeScale = 0;
            SoundManager.Instance.PlayButton();
            pauseMenu.SetActive(true);
        }

        public void BackToGame()
        {
            Time.timeScale = 1;
            SoundManager.Instance.PlayButton();
            pauseMenu.SetActive(false);
        }

        public void LoadStage1()
        {
            SoundManager.Instance.PlayButton();
            SceneManager.LoadScene(1);
        }

        public void LoadStage2()
        {
            SoundManager.Instance.PlayButton();
            SceneManager.LoadScene(2);
        }
        
        public void LoadStage3()
        {
            SoundManager.Instance.PlayButton();
            SceneManager.LoadScene(3);
        }
        public void LoadStage4()
        {
            SoundManager.Instance.PlayButton();
            SceneManager.LoadScene(4);
        }
        public void LoadStage5()
        {
            SoundManager.Instance.PlayButton();
            SceneManager.LoadScene(5);
        }
    }
}
