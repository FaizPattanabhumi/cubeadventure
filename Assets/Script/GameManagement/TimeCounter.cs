using UnityEngine;
using UnityEngine.UI;

namespace Script.GameManagement
{
    public class TimeCounter : MonoBehaviour
    {

        //time Get only
        public float time { get; private set; }
        
        [SerializeField] private Text timeText;
        [SerializeField] public bool allowCount;
        public GameObject timerGameObj;
        public GameManager gameManager;

        void Start()
        {
            gameManager = FindObjectOfType<GameManager>();
            time = 0;
        }

        public void StartCount()
        {
            allowCount = true;
            Debug.Log("Timer Count Started");
        }
        
        public void StopCount()
        {
            Debug.Log("Timer Count Stoped");
            allowCount = false;
        }

        void Update()
        {
            CountCheck();
            timeText.text = $"Time : {(int) time}s";
            
            if (allowCount == true)
            {
                time += Time.deltaTime;
            }
            
        }

        void CountCheck()
        {
            if (gameManager.gameIsOver == true)
            {
                StopCount();
            }
        }

        public void ResetTime()
        {
            time = 0;
        }
    }
}
