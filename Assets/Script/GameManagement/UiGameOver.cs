using Script.Player;
using UnityEngine;
using UnityEngine.UI;

namespace Script.GameManagement
{
    public class UiGameOver : MonoSingleton<UiGameOver>
    {
        public GameObject UiGameObj;
        
        [SerializeField] private Text timeText;
        [SerializeField] private Text dieCountText;
        [SerializeField] private TimeCounter timeCounter;
        [SerializeField] private MainCharacter mainPlayer;
        [SerializeField] private StageManager stage;
        void Awake()
        {

            UiGameObj = gameObject;
        }
        
        void Update()
        {
            timeText.text = $"{(int)timeCounter.time}";
            dieCountText.text = $"{stage.dieTimeCount}";
        }

    }
}
