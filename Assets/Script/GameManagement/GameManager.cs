using UnityEngine;

namespace Script.GameManagement
{
    public class GameManager : MonoSingleton<GameManager>
    {

        [SerializeField] public GameObject startMenu;
        [SerializeField] public GameObject stageMenu;
        [SerializeField] private GameObject endGameMenu;
        [SerializeField] public GameObject timeCounterObj;
        [SerializeField] public bool gameIsOver;


        protected void Awake()
        {
            startMenu = GameObject.Find("StartMenu");
            stageMenu = GameObject.Find("StageMenu");
            
            DontDestroyOnLoad(gameObject);
            

        }
        public void Start()
        {
            startMenu.SetActive(true);
            stageMenu.SetActive(false);
            gameIsOver = false;
            timeCounterObj.SetActive(false);
            UiGameOver.Instance.UiGameObj.SetActive(false);
        }

        public void PlayGame()
        {
            gameIsOver = false;
            
            //Hide play menu
            startMenu.SetActive(false);
            
            //show select stage
            stageMenu.SetActive(true);

        }

        public void BackToPlayMenu()
        {
            //Hide select menu
            startMenu.SetActive(true);

            //show play menu
            stageMenu.SetActive(false);

        }

        public void GameOver()
        {
            Debug.Log("!!Game End!!");
            Time.timeScale = 0;

            gameIsOver = true;
            //Stop time counter
            timeCounterObj.GetComponent<TimeCounter>().StopCount();

            
            //Show score UI
            endGameMenu.SetActive(true);
            
            //Show time counter
            endGameMenu.SetActive(true);
            
        }

        public void GameStart()
        {
            Time.timeScale = 1;
            gameIsOver = false;
            
            //Reset Timer
            timeCounterObj.GetComponent<TimeCounter>().ResetTime();
            
            //Show Timer Ui
            timeCounterObj.SetActive(true);
            
            //Start Count Time
            timeCounterObj.GetComponent<TimeCounter>().StartCount();
            
            Debug.Log("Game Start!!");
        }

        public void DisableFullScreen()
        {
            if (Screen.fullScreen == false)
            {
                Screen.fullScreen = true;
            }
            else
            {
                Screen.fullScreen = false;
            }
        }

        public void QuitGame()
        {
            Application.Quit();
        }
        
        
    }
}
