﻿using UnityEngine;

namespace Script.GameManagement
{
    public class SoundManager : MonoSingleton<SoundManager>
    {
        [SerializeField] private AudioClip falloutSound;
        [SerializeField] private AudioClip jumpSound;
        [SerializeField] private AudioClip buttonSound;
        [SerializeField] private AudioSource playerAudioSource;
        [SerializeField] private AudioSource mainAudioSource;

        void Start()
        {
            playerAudioSource = FindObjectOfType<AudioSource>();
            
            Debug.Assert(falloutSound == null,"falloutSound = null");
            Debug.Assert(jumpSound == null,"jumpSound = null");
            Debug.Assert(buttonSound == null,"buttonSound = null");
            Debug.Assert(playerAudioSource == null,"playerAudioSource = null");
            Debug.Assert(mainAudioSource == null,"mainAudioSource = null");

        }
        
        public void PlayerFall()
        {
            PlaySound(playerAudioSource,falloutSound);
        }
        
        public void PlayerJump()
        {
            PlaySound(playerAudioSource,jumpSound);
        }
        
        public void PlayButton()
        {
            PlaySound(mainAudioSource,buttonSound);
        }
        
        public void PlaySound(AudioSource audioSource, AudioClip sound)
        {
            audioSource.clip = sound;
            audioSource.Play();
        }


        
    }
}
