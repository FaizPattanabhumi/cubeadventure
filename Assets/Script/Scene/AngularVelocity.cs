using UnityEngine;

namespace Script.Scene
{
    public class AngularVelocity : MonoBehaviour
    {
        
        [SerializeField] private Rigidbody rb;
        [SerializeField] private Vector3 angularVelocity;
    
        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }
    
        void FixedUpdate()
        {
            rb.angularVelocity = angularVelocity;
        }
    }
}

