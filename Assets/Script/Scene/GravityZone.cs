using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Script.Scene
{
    public class GravityZone : MonoBehaviour
    {
        [SerializeField] private GameObject player;
        [SerializeField] private Vector3 gravityDir;
        [SerializeField] private Vector3 normalGravityDir;

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject == player)
            {
                Debug.Log("Gravity Changed");
                Physics.gravity = gravityDir;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject == player)
            {
                Debug.Log("Gravity Changed To normal");
                Physics.gravity = normalGravityDir;
            }
        }
    }
}
