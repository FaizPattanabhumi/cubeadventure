using System;
using UnityEngine;

namespace Script.Scene
{
    public class AttractZone : MonoBehaviour
    {
        [SerializeField] private Rigidbody selfRigidbody;
        [SerializeField] private GameObject player;
        [SerializeField] private float gravity = 6.67f;

        private void Start()
        {
            selfRigidbody = GetComponent<Rigidbody>();
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject == player)
            {
                AttractZone[] attractors = FindObjectsOfType<AttractZone>();
    
                foreach (AttractZone allPlanet in attractors)
                {
                    if (allPlanet != this)
                    {
                        Attraction(allPlanet);
                    }
                }
            }
        }
    
        void Attraction(AttractZone objToAttractor)
        {
            Rigidbody mRigidToAttraction = objToAttractor.selfRigidbody;
    
            Vector3 direction = selfRigidbody.position - mRigidToAttraction.position;
    
            float distance = direction.magnitude;
    
            float forceMagnitude = gravity * ((selfRigidbody.mass * mRigidToAttraction.mass) / (distance*distance));
    
            Vector3 force = direction.normalized * forceMagnitude;
            
            mRigidToAttraction.AddForce(force);
        }
    
        void FixedUpdate()
        {
            AttractZone[] attractors = FindObjectsOfType<AttractZone>();
    
            foreach (AttractZone allPlanet in attractors)
            {
                if (allPlanet != this)
                {
                    Attraction(allPlanet);
                }
            }
        }
    }
}
