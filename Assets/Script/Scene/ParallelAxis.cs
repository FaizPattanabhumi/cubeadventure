using UnityEngine;

namespace Script.Scene
{
    public class ParallelAxis : MonoBehaviour
    {

        [SerializeField] private Rigidbody rb;
        [SerializeField] private float rotateSpeed;

        void FixedUpdate()
        {
            rb.angularVelocity = rb.centerOfMass * Time.deltaTime * rotateSpeed;
        }

    }
}
