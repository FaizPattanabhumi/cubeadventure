using UnityEngine;

namespace Script.Scene
{
    public class ProjectileLauncher : MonoBehaviour
    {
        [SerializeField] private GameObject targetPosition;
        [SerializeField] private GameObject originPosition;
        [SerializeField] private GameObject playerObj;
        [SerializeField] private Rigidbody playerRigid;
        [SerializeField] private float time;


        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == playerObj)
            {
                originPosition.transform.position = playerObj.transform.position;
                LaunchProjectile();
            }
        }

        void LaunchProjectile()
        {
            playerRigid.velocity =
                VelocityCalculate(targetPosition.transform.position, originPosition.transform.position, time);
        }

        Vector3 VelocityCalculate(Vector3 target, Vector3 origin, float timeToTarget)
        {
            Vector3 distance = target - origin;
            Vector3 distanceXZPlane = distance;

            distanceXZPlane.y = 0;
            float distanceXZ = distanceXZPlane.magnitude;
            float distanceY = distance.y;

            float vXz = distanceXZ / timeToTarget;
            float vY = distanceY / timeToTarget + 0.5f * Mathf.Abs(Physics.gravity.y) * timeToTarget;

            Vector3 result = distanceXZPlane.normalized;
            result *= vXz;
            result.y = vY;

            return result;
        }
    }
}
