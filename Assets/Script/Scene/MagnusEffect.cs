using UnityEngine;

namespace Script.Scene
{
    public class MagnusEffect : MonoBehaviour
    {

        [SerializeField] private Rigidbody selfRigidbody;
        [SerializeField] private Vector3 angularVelocity;
        [SerializeField] private Vector3 velocity;
        
        private void FixedUpdate()
        {

            selfRigidbody.angularVelocity = angularVelocity;
            selfRigidbody.velocity = velocity;


            selfRigidbody.AddForce(Vector3.Cross(angularVelocity, velocity));
        }

    }
}
