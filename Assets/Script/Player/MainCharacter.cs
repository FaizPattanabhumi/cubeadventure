using Script.GameManagement;
using UnityEngine;

namespace Script.Player
{
    public class MainCharacter : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private Collider goalCollider;

        private void Awake()
        {
            Debug.Assert(goalCollider == null,"goalCollider = null");
            Debug.Assert(gameManager == null,"GameManager = null");
        }

        void Start()
        {
            gameManager.GameStart();
            Debug.Log("Player Start");
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other == goalCollider)
            {
                Debug.Log("Game Over");
                gameManager.GameOver();
            }
        }
        

        
    }
}
