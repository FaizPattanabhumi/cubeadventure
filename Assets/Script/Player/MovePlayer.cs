﻿using Script.GameManagement;
using UnityEngine;

namespace Script.Player
{
    public class MovePlayer : MonoBehaviour
    {
        [SerializeField] private float force;
        [SerializeField] private float mass;
        [SerializeField] private float acceleration;

        private Rigidbody rb;

        void Awake()
        {
            Physics.gravity = new Vector3(0,-9.81f,0);
        }
        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

        void Update()
        {
            CharacterControl();
        }



        void CharacterControl()
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                GoUp();
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                GoLeft();
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                GoRight();
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                GoDown();
            }
        }

        void CalculateForce()
        {
            mass = rb.mass;
            force = mass * acceleration;
        }

        public void GoUp()
        {
            SoundManager.Instance.PlayerJump();
            CalculateForce();
            rb.AddForce(0, force, force);
        }

        public void GoLeft()
        {
            SoundManager.Instance.PlayerJump();
            CalculateForce();
            rb.AddForce(-force, force, 0);
        }

        public void GoRight()
        {
            SoundManager.Instance.PlayerJump();
            CalculateForce();
            rb.AddForce(force, force, 0);
        }

        public void GoDown()
        {
            SoundManager.Instance.PlayerJump();
            CalculateForce();
            rb.AddForce(0, force, -force);
        }
    }
}
